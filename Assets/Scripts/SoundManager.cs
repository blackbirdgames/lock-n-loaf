﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Handles listening to events and playing soundclips in correct channel
public class SoundManager : MonoBehaviour {

    public static SoundManager instance;

    // Reference all unique audio clips
    public AudioClip genericButton, newLevel, newWave,
        breadSpawned, breadGrabbed, breadReleased, breadCollision,
        foodCollision, sandwichMade, scoreUpdated, gameMusic, menuMusic;

    public AudioSource SFXChannel, BGMChannel;

    public float pitchVariance = .07f;

    // Listen to events
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            DestroyImmediate(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        EventManager.instance.OnButtonPress.AddListener(OnButtonPress);
        EventManager.instance.OnGamePaused.AddListener(OnGamePaused);
        EventManager.instance.OnGameUnpaused.AddListener(OnGameUnpaused);
        EventManager.instance.OnMainMenu.AddListener(OnMainMenu);
        EventManager.instance.OnNewLevel.AddListener(OnNewLevel);
        EventManager.instance.OnNewWave.AddListener(OnNewWave);
        EventManager.instance.OnLevelComplete.AddListener(OnLevelComplete);
        EventManager.instance.OnLevelFail.AddListener(OnLevelFail);
        EventManager.instance.OnBreadSpawn.AddListener(OnBreadSpawn);
        EventManager.instance.OnBreadGrabbed.AddListener(OnBreadGrabbed);
        EventManager.instance.OnBreadReleased.AddListener(OnBreadReleased);
        EventManager.instance.OnIngredientCollision.AddListener(OnIngredientCollision);
        EventManager.instance.OnBreadCollision.AddListener(OnBreadCollision);
        EventManager.instance.OnSandwichMade.AddListener(OnSandwichMade);
    }

    private void Start()
    {
        BGMChannel.Stop();
        //BGMChannel.clip = menuMusic;
        //BGMChannel.Play();
    }

    void PlaySFX(AudioClip _clip)
    {
        SFXChannel.pitch = Random.Range(1f + pitchVariance, 1f - pitchVariance);
        SFXChannel.PlayOneShot(_clip);
    }

    //EVENTS
    void OnButtonPress(string _buttonName)
    {
        PlaySFX(genericButton);
    }

    void OnGamePaused()
    {
        PlaySFX(genericButton);
        BGMChannel.Pause();
    }

    void OnGameUnpaused()
    {
        PlaySFX(genericButton);
        BGMChannel.clip = gameMusic;
        BGMChannel.Play();
    }

    // If not playing menu music play it
    void OnMainMenu()
    {
        //PlaySFX(genericButton);
        if (BGMChannel.clip != menuMusic)
        {
            BGMChannel.clip = menuMusic;
            BGMChannel.Play();
        }
    }

    void OnNewLevel(int _level)
    {
        PlaySFX(newLevel);
        BGMChannel.Stop();
        BGMChannel.clip = gameMusic;
        BGMChannel.Play();
    }

    void OnNewWave(int _wave)
    {
        PlaySFX(newWave);
    }

    void OnLevelComplete()
    {
        PlaySFX(genericButton);
    }

    void OnLevelFail()
    {
        PlaySFX(genericButton);
        BGMChannel.clip = menuMusic;
    }

    void OnBreadSpawn(int _ammo)
    {
        PlaySFX(breadSpawned);
    }

    void OnBreadGrabbed(Vector3 _pos)
    {
        PlaySFX(breadGrabbed);
    }

    void OnBreadReleased(Vector3 _pos)
    {
        PlaySFX(breadReleased);
    }

    void OnIngredientCollision(Ingredient _ingredient, Bread _bread)
    {
        PlaySFX(foodCollision);
    }

    void OnBreadCollision(Bread _breadL, Bread _breadR)
    {
        PlaySFX(breadCollision);
    }

    void OnSandwichMade(Vector3 _pos, int _score)
    {
        PlaySFX(sandwichMade);
        PlaySFX(scoreUpdated);
    }

    // Remove event subscriptions if this gameobject is destroyed
    void OnDestroy()
    {
        EventManager.instance.OnButtonPress.RemoveListener(OnButtonPress);
        EventManager.instance.OnGamePaused.RemoveListener(OnGamePaused);
        EventManager.instance.OnGameUnpaused.RemoveListener(OnGameUnpaused);
        EventManager.instance.OnMainMenu.RemoveListener(OnMainMenu);
        EventManager.instance.OnNewLevel.RemoveListener(OnNewLevel);
        EventManager.instance.OnNewWave.RemoveListener(OnNewWave);
        EventManager.instance.OnLevelComplete.RemoveListener(OnLevelComplete);
        EventManager.instance.OnLevelFail.RemoveListener(OnLevelFail);
        EventManager.instance.OnBreadSpawn.RemoveListener(OnBreadSpawn);
        EventManager.instance.OnBreadGrabbed.RemoveListener(OnBreadGrabbed);
        EventManager.instance.OnBreadReleased.RemoveListener(OnBreadReleased);
        EventManager.instance.OnIngredientCollision.RemoveListener(OnIngredientCollision);
        EventManager.instance.OnBreadCollision.RemoveListener(OnBreadCollision);
        EventManager.instance.OnSandwichMade.RemoveListener(OnSandwichMade);
    }
}
