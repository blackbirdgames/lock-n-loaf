﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq; // Need this to find number of distinct members in a list!

// Handles scoring and ammo
// https://www.codeproject.com/Questions/456824/How-to-Count-Distinct-in-a-list-in-csharp
public class GameManager : MonoBehaviour {

    public static GameManager instance;
    //  This is a hack
    public int startingAmmo = 25;//GO TO LEVEL MANAGER
    public int ammo = 0;//GO TO LEVEL MANAGER
    int healthScore;
    int nutrition;
    int calories;
    List<Ingredient> currentSandwich = new List<Ingredient>();
    int currentLevel;
    GameObject gameBackground;

    // Use this for initialization, listen to events
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            DestroyImmediate(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        EventManager.instance.OnMainMenu.AddListener(OnMainMenu);
        EventManager.instance.OnNewLevel.AddListener(OnNewLevel);
        EventManager.instance.OnBreadCollision.AddListener(OnBreadCollision);
        EventManager.instance.OnSandwichMade.AddListener(OnSandwichMade);
        EventManager.instance.OnIngredientCollision.AddListener(OnIngredientCollision);

        gameBackground = GetComponentInChildren<SpriteRenderer>().gameObject;
    }

    void Start()
    {
        // Spawn bread on game start regardless of ammo
        EventManager.instance.OnBreadSpawn.Invoke(ammo);
        gameBackground.SetActive(false);
    }

    //EVENTS
    void OnMainMenu()
    {
        ammo = 0;
        Time.timeScale = 1f;
        Bread[] breads = GameObject.FindObjectsOfType<Bread>();
        foreach(Bread bread in breads)
        {
            Destroy(bread.gameObject);
        }
        gameBackground.gameObject.SetActive(false);
    }
    void OnNewLevel(int _levelIndex)
    {
        ammo = startingAmmo;
        EventManager.instance.OnBreadSpawn.Invoke(ammo);
        gameBackground.gameObject.SetActive(true);
    }

    // 
    void OnSandwichMade(Vector3 _pos, int _i)
    {
        // Update score
        if (ammo >= 2)
        {
            ammo -= 2;
            EventManager.instance.OnBreadSpawn.Invoke(ammo);
        }
    }

    public void OnIngredientCollision(Ingredient _ingredient, Bread _bread)
    {
        currentSandwich.Add(_ingredient);
    }

    // Create new health score and feed it into sandwich made event
    public void OnBreadCollision(Bread _breadL, Bread _breadR)
    {
        nutrition = 0;
        calories = 0;

        // calculate score
        foreach (Ingredient i in currentSandwich)
        {
            nutrition += i.nutrition;
            calories += i.calories;
        }

        int multiplier = (from x in currentSandwich select x).Distinct().Count();
        nutrition *= multiplier;
        healthScore = nutrition - calories;

        EventManager.instance.OnSandwichMade.Invoke(_breadL.transform.position, healthScore);
        currentSandwich.Clear();
    }

    // Remove event subscriptions if this gameobject is destroyed
    void OnDestroy()
    {
        EventManager.instance.OnMainMenu.RemoveListener(OnMainMenu);
        EventManager.instance.OnNewLevel.RemoveListener(OnNewLevel);
        EventManager.instance.OnBreadCollision.RemoveListener(OnBreadCollision);
        EventManager.instance.OnSandwichMade.RemoveListener(OnSandwichMade);
        EventManager.instance.OnIngredientCollision.RemoveListener(OnIngredientCollision);
    }
}
