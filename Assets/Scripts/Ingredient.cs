﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Hold specific ingredient info - maybe this could be a struct idfk
public class Ingredient : MonoBehaviour {

    public int nutrition = 10;
    public int calories = 5;
    public Color juiceColour = Color.magenta;
    //public ParticleSystem startingParticles;

	// Use this for initialization
	void Start ()
    {
        //juiceParticles.startColor = juiceColour;
	}
	
	// Update is called once per frame
	void Update ()
    {
		// Animate here if too lazy to use Unity animator
	}

    void DestroyThisIngredient()
    {
        //Handles everything required before destruction
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bread"))
        {
            Debug.Log("Ingredient hit");
            // Ivoke ingredient collision event feeding this ingredient
            EventManager.instance.OnIngredientCollision.Invoke(this, other.gameObject.GetComponent<Bread>());
            // Destroy this ingredient
            // THIS IS A HACK
            //Destroy(Instantiate(juiceParticles, transform.position, Quaternion.identity), 3f);
            Destroy(gameObject);
        }
    }
}
