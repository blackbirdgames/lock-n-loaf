﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Handles homing in on other bread tags and collisions with bread and ingredients
public class Bread : MonoBehaviour {

    public float speed = 5f;
    public bool isReleased = false;
    public bool isGrabbed = false;
    public int breadIndex = 0;

    Rigidbody myRigidbody;
    GameObject target;

    GameObject deathParticles;

	// Use this for initialization
	void Start ()
    {
        myRigidbody = GetComponent<Rigidbody>();
        deathParticles = GetComponentInChildren<ParticleSystem>().gameObject;
        if(deathParticles != null)
        {
            Debug.Log("Found deathparticles");
        }
	}
	
	void Update ()
    {
        // If released is true, find other bread and move toward
        if (isReleased)
        {
            GameObject[] breads = GameObject.FindGameObjectsWithTag("Bread");
            foreach(GameObject bread in breads)
            {
                if (bread != gameObject && bread != null)
                {
                    target = bread;
                }
            }

            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
            //myRigidbody.MovePosition(transform.position + transform.up * Time.deltaTime * speed);
        }
	}

    //TOUCH MESSAGES
    void OnTouchBegan(TouchManager.TouchData _td)
    {

    }

    void OnTouchMoved(TouchManager.TouchData _td)
    {

    }

    void OnTouchStay(TouchManager.TouchData _td)
    {

    }

    void OnTouchEnd(TouchManager.TouchData _td)
    {

    }

    void OnTouchCancel(TouchManager.TouchData _td)
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bread"))
        {
            //deathParticles.GetComponent<ParticleSystem>().Play();
            //deathParticles.transform.parent = null;
            Destroy(Instantiate(deathParticles, transform.position, Quaternion.identity), 3f);

            // If this is the first bread is scene
            if(breadIndex == 0)
            {
                EventManager.instance.OnBreadCollision.Invoke(this, other.GetComponent<Bread>());
                Destroy(gameObject);
                Destroy(other.gameObject);
            }

            
        }
    }
}
