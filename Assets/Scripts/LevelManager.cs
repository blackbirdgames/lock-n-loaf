﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    public List<GameObject[]> levels;
    public float waveTime = 4f;
    float timer;
    bool inGame;
    int currentWave;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            DestroyImmediate(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        EventManager.instance.OnNewLevel.AddListener(OnNewLevel);
        EventManager.instance.OnNewWave.AddListener(OnNewWave);
        EventManager.instance.OnMainMenu.AddListener(OnMainMenu);

    }

    public void Update()
    {
        if (inGame)
        {
            timer += Time.deltaTime;
            if(timer >= waveTime)
            {
                EventManager.instance.OnNewWave.Invoke(currentWave);
            }
        }
    }

    // EVENTS
    void OnNewLevel(int _levelIndex)
    {
        timer = 0;
        inGame = true;
        currentWave = 0;
    }

    void OnNewWave(int _waveIndex)
    {
        timer = 0;
        ++currentWave;
    }

    void OnMainMenu()
    {
        inGame = false;
    }

    void OnDestroy()
    {
        EventManager.instance.OnNewLevel.RemoveListener(OnNewLevel);
        EventManager.instance.OnNewWave.RemoveListener(OnNewWave);
        EventManager.instance.OnMainMenu.RemoveListener(OnMainMenu);
    }
}
