﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Handles moving and playing particle systems by listening to events - probably only needs to exist in game scene 
public class ParticleManager : MonoBehaviour {

    public static ParticleManager instance;

    ParticleSystem juiceParticles;
    ParticleSystem.MainModule juiceMain;

    // Subscribe to events and reference main module on juiceParticles
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            DestroyImmediate(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        EventManager.instance.OnIngredientCollision.AddListener(OnIngredientCollision);

        juiceParticles = GetComponent<ParticleSystem>();
        juiceMain = juiceParticles.main;
    }

    //EVENTS
    void OnIngredientCollision(Ingredient _ingredient, Bread _bread)
    {
        juiceMain.startColor = _ingredient.juiceColour;
        juiceParticles.transform.position = _bread.transform.position;
        juiceParticles.Play();
    }

    // Remove event subscriptions if this gameobject is destroyed
    void OnDestroy()
    {
        EventManager.instance.OnIngredientCollision.RemoveListener(OnIngredientCollision);
    }
}
