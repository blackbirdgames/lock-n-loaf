﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;// Use this to reference files for import

// https://docs.unity3d.com/Manual/class-TextAsset.html
// https://msdn.microsoft.com/en-us/library/xfhwa508(v=vs.110).aspx // Dictionaries
public class LevelGenerator : MonoBehaviour
{
    public static LevelGenerator instance;

    private string filePath;
    string fileData;

    // THIS NEEDS TO BE CHANGED TO AN OPEN-ENDED ARRAY OF GAME OBJECTS FOR EXPANDABILITY
    public GameObject EmptyPrefab, AvocadoPrefab,TomatoPrefab, CheesePrefab,PicklePrefab,OnionPrefab,ButterPrefab,PepperoniPrefab,
    NutellaPrefab,PeanutbutterPrefab,BaconPrefab;

    public List<GameObject[]> waves;

    float timer;
    bool inGame;
    int currentWave;
    public float waveTime = 4f;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            DestroyImmediate(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        EventManager.instance.OnNewLevel.AddListener(OnNewLevel);
        EventManager.instance.OnNewWave.AddListener(OnNewWave);
        EventManager.instance.OnMainMenu.AddListener(OnMainMenu);

    }

    public void Update()
    {
        if (inGame)
        {
            timer += Time.deltaTime;
            if (timer >= waveTime && currentWave < waves.Count)
            {
                EventManager.instance.OnNewWave.Invoke(currentWave);
            }
        }
    }

    // Load text from csv in resource folder
    public void LoadCSV(int _levelIndex)
    {
        TextAsset csvText = (TextAsset)Resources.Load("Level " + _levelIndex, typeof(TextAsset));
        fileData = csvText.text;

        string[] lines = fileData.Split('\n');
        
        waves = new List<GameObject[]>();
        List<GameObject> wave = new List<GameObject>();
        
        List<string> gridLines = new List<string>(lines);
        for (int z = 0; z < gridLines.Count; z++)
        {
            List<string> currentRow = new List<string>(gridLines[z].Split(','));
            for (int x = 0; x < currentRow.Count; x++)
            {
                if (x == 0 && currentRow[x] == "WE") // THIS WAVEBREAK CODE NEEDS TO BE IN README!
                {
                    waves.Add(wave.ToArray());
                    wave = new List<GameObject>();
                }
                string currentGrid = currentRow[x];

                if (currentGrid.IndexOf('\r') != -1)
                {
                    currentGrid = currentGrid.Substring(0, currentGrid.IndexOf('\r'));
                }

                // THIS NEEDS TO BE REPLACED WITH A DICTIONARY LOOKUP FOR EXPANDABILITY!
                switch(currentGrid)
                {
                    case "A":
                        wave.Add(AvocadoPrefab);
                        break;
                    case "T":
                        wave.Add(TomatoPrefab);
                        break;
                    case "C":
                        wave.Add(CheesePrefab);
                        break;
                    case "P":
                        wave.Add(PicklePrefab);
                        break;
                    case "O":
                        wave.Add(OnionPrefab);
                        break;
                    case "B":
                        wave.Add(ButterPrefab);
                        break;
                    case "PP":
                        wave.Add(PepperoniPrefab); 
                        break;
                    case "N":
                        wave.Add(NutellaPrefab);
                        break;
                    case "PB":
                        wave.Add(PeanutbutterPrefab);
                        break;
                    case "BC":
                        wave.Add(BaconPrefab);
                        break;
                    case "":
                        wave.Add(EmptyPrefab);
                        break;
                }
            }
        }

        // Debug by foreach the list of gameobject arrays
        foreach(GameObject[] gOs in waves)
        {
            string levelString = "";
            foreach(GameObject gO in gOs)
            {
                levelString += gO.name + ", ";
            }
            Debug.Log(levelString);
        }
    }

    // Spawn new wave clear last
    public void SpawnNewWave(int _waveIndex)
    {
        // Clear existing objects
        DestroyAllChildren();

        GameObject[] gOs = waves[_waveIndex];
        int xAdjust = 0;
        int yAdjust = 0;

        int xCurrent = -2;
        int yCurrent = 2;

        // Instantiate as child with postition dictated by xy itterations
        foreach (GameObject gO in gOs)
        {
            Instantiate(gO, new Vector3(xCurrent + xAdjust, yCurrent - yAdjust, 0), Quaternion.identity, transform);// Parent to level manager to tweak location of grid and animate etc.
            gO.transform.position += transform.position; // Add this object's transform position

            if (xAdjust == 4)
            {
                xAdjust = 0;
                yAdjust++;
            }
            else
            {
                xAdjust++;
            }
        }
    }

    // Destroy all objects in children of this object
    void DestroyAllChildren()
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    // EVENTS
    void OnNewLevel(int _levelIndex)
    {
        LoadCSV(_levelIndex);
        timer = 0;
        inGame = true;
        currentWave = 0;
    }

    void OnNewWave(int _waveIndex)
    {
        SpawnNewWave(_waveIndex);
        timer = 0;
        ++currentWave;
        if (currentWave == waves.Count)
        {
            // Wait for wave time first!
            EventManager.instance.OnLevelComplete.Invoke();
        }
    }

    void OnMainMenu()
    {
        DestroyAllChildren();
        inGame = false;
    }

    void OnDestroy()
    {
        EventManager.instance.OnNewLevel.RemoveListener(OnNewLevel);
        EventManager.instance.OnNewWave.RemoveListener(OnNewWave);
        EventManager.instance.OnMainMenu.RemoveListener(OnMainMenu);
    }
}
