﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// Fires events and updates UI objects based on button presses - use this class to reference buttons
public class UIController : MonoBehaviour
{
    public static UIController instance;

    public Text totalScore, infoText, ammoText;

    Animator animator;

    int currentLevel;
    int currentScore;

    public GameObject startPanel, mainMenuPanel, levelSelectPanel, creditsPanel, 
        gamePanel, pausePanel, levelCompletePanel, levelFailPanel;

    [HideInInspector]
    public string buttonName;

    [HideInInspector]
    public enum State
    {
        StartScreen, MainMenu, LevelSelect, Credits,
        Game, PauseMenu, LevelComplete, LevelFail, Quit
    }

    //[HideInInspector]
    public State currentState;

    // Subscribe to events
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            DestroyImmediate(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        animator = GetComponent<Animator>();

        EventManager.instance.OnBreadSpawn.AddListener(OnBreadSpawn);
        EventManager.instance.OnSandwichMade.AddListener(OnSandwichMade);
        EventManager.instance.OnLevelComplete.AddListener(OnLevelComplete);
        EventManager.instance.OnBreadCollision.AddListener(OnBreadCollision);
    }

    // On game start, set state to start
    void Start()
    {
        ChangeGameState(State.StartScreen);
    }

    // Buttons
    public void GenericButton(string buttonName)
    {
        EventManager.instance.OnButtonPress.Invoke(buttonName);

        switch (buttonName)
        {
            case "Pause":
                ChangeGameState(State.PauseMenu);
                break;
            case "Resume":
                ChangeGameState(State.Game);
                break;
            case "LevelSelect":
                ChangeGameState(State.LevelSelect);
                break;
            case "Quit":
                ChangeGameState(State.Quit);
                break;
            case "Credits":
                ChangeGameState(State.Credits);
                break;
            case "MainMenu":
                ChangeGameState(State.MainMenu);
                break;
            case "Retry":
                ChangeGameState(State.Game);
                break;
            case "Next":
                ++currentLevel;
                ChangeGameState(State.Game);
                break;
        }
    }

    // Level buttons
    public void SelectLevelButton(int _level)
    {
        currentLevel = _level;
        ChangeGameState(State.Game);
    }

    // Start state manager method based on game state selection from other scripts
    public void ChangeGameState(State _newState)
    {
        // Clear panels
        startPanel.SetActive(false);
        mainMenuPanel.SetActive(false);
        levelSelectPanel.SetActive(false);
        creditsPanel.SetActive(false);
        gamePanel.SetActive(false);
        pausePanel.SetActive(false);
        levelCompletePanel.SetActive(false);
        levelFailPanel.SetActive(false);

        switch (_newState)
        {
            case State.StartScreen:
                Debug.Log("State changed to StartScreen");
                startPanel.SetActive(true);
                // INVOKE EVENTS AND ANIMATIONS HERE *********************************
                break;
            case State.MainMenu:
                Debug.Log("State changed to MainMenu");
                mainMenuPanel.SetActive(true);
                EventManager.instance.OnMainMenu.Invoke();
                if (currentState == State.PauseMenu)
                {
                    Time.timeScale = 1f;
                }
                break;
            case State.Credits:
                Debug.Log("State changed to Credits");
                creditsPanel.SetActive(true);
                mainMenuPanel.SetActive(true);
                break;
            case State.LevelSelect:
                Debug.Log("State changed to LevelSelect");
                levelSelectPanel.SetActive(true);
                mainMenuPanel.SetActive(true);
                break;
            case State.Game:
                Debug.Log("State changed to Game");
                gamePanel.SetActive(true);
                if(currentState != State.PauseMenu)
                {
                    EventManager.instance.OnGameUnpaused.Invoke();
                    currentScore = 0;
                    EventManager.instance.OnNewLevel.Invoke(currentLevel);
                }
                else
                {
                    Time.timeScale = 1f;
                }
                break;
            case State.PauseMenu:
                Debug.Log("State changed to PauseMenu");
                pausePanel.SetActive(true);
                gamePanel.SetActive(true);
                Time.timeScale = 0f;
                EventManager.instance.OnGamePaused.Invoke();
                break;
            case State.LevelComplete:
                Debug.Log("State changed to LevelWin");
                levelCompletePanel.SetActive(true);
                gamePanel.SetActive(true);
                break;
            case State.LevelFail:
                Debug.Log("State changed to LevelFail");
                levelFailPanel.SetActive(true);
                gamePanel.SetActive(true);
                break;
            case State.Quit:
                Debug.Log("State changed to quit");
                //quitPanel.SetActive(true);
                Application.Quit();
                break;
        }
        currentState = _newState;
    }

    // EVENTS ************************************************************************
    void OnBreadSpawn(int _ammo)
    {
        ammoText.text = "x " + _ammo;
    }

    void OnSandwichMade(Vector3 _pos, int _score)
    {
        currentScore += _score;
        // UPDATE POPUP TEXT HERE WITH _score
        // Update total score text here
        totalScore.text = "SCORE: " + currentScore.ToString("D6");
    }

    void OnBreadCollision(Bread _bread1, Bread _bread2)
    {
        if(currentState == State.StartScreen)
        {
            ChangeGameState(State.MainMenu);
        }
    }

    // 
    void OnLevelComplete()
    {
        ChangeGameState(State.LevelComplete);
    }

    // Remove event subscriptions if this gameobject is destroyed
    void OnDestroy()
    {
        EventManager.instance.OnBreadSpawn.RemoveListener(OnBreadSpawn);
        EventManager.instance.OnSandwichMade.RemoveListener(OnSandwichMade);
        EventManager.instance.OnLevelComplete.RemoveListener(OnLevelComplete);
        EventManager.instance.OnBreadCollision.RemoveListener(OnBreadCollision);
    }
}
