﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Handles moving a single bread based on touch input
public class BreadController : MonoBehaviour {

    public int breadSpawnIndex;
    public Bread breadPrefab;
    [HideInInspector]
    public Bread myBread;
    public float grabDistance = 2f;

    // Use this for initialization, listen to events
    void Awake()
    {
        EventManager.instance.OnBreadSpawn.AddListener(OnBreadSpawn);
        EventManager.instance.OnBreadCollision.AddListener(OnBreadCollision);
    }

    //TOUCH MESSAGES
    void OnTouchBegan(TouchManager.TouchData _td)
    {
        Debug.Log("Touchpad touched");
        if(myBread != null && _td.position.y < grabDistance && _td.position.y >-grabDistance)
        {
            Debug.Log("Bread touched");
            myBread.isGrabbed = true;
            EventManager.instance.OnBreadGrabbed.Invoke(myBread.transform.position);
        }
    }

    void OnTouchMoved(TouchManager.TouchData _td)
    {
        
        if (myBread != null && myBread.isGrabbed && !myBread.isReleased)
        {
            Debug.Log("Touchpad swiped");
            myBread.transform.position = new Vector3
                (myBread.transform.position.x, _td.position.y, myBread.transform.position.z);
        }
    }

    void OnTouchStay(TouchManager.TouchData _td)
    {

    }

    void OnTouchEnd(TouchManager.TouchData _td)
    {
        Debug.Log("Finger lifted from touchpad");
        if (myBread != null && myBread.isGrabbed && !myBread.isReleased)
        {
            Debug.Log("Bread released");
            myBread.isReleased = true;
            EventManager.instance.OnBreadReleased.Invoke(myBread.transform.position);
            myBread = null;
        }
    }

    void OnTouchCancel(TouchManager.TouchData _td)
    {
        
    }

    //EVENTS
    void OnBreadSpawn(int _ammo)
    {
        myBread = Instantiate(breadPrefab, transform.position, transform.rotation) as Bread;
        myBread.breadIndex = breadSpawnIndex;
    }

    void OnBreadCollision(Bread _bread1, Bread _bread2)
    {
        //if (myBread != null)
        //{
        //    myBread = null;
        //}
    }

    // Remove events
    void OnDestroy()
    {
        EventManager.instance.OnBreadSpawn.RemoveListener(OnBreadSpawn);
        EventManager.instance.OnBreadCollision.AddListener(OnBreadCollision);
    }
}
