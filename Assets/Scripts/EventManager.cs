﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events; // Required to access unity events

// Static instance not attached to gameobject holds events to be invoked or listened to by others
// No need to inherit from monobehaviour
public class EventManager
{
    private static EventManager i;
    public static EventManager instance
    {
        get
        {
            if (i == null)
                i = new EventManager();
            return i;
        }
    }

    // EVENTS - reference in this classes namespace to access via instance
    public UnityEvent OnMainMenu = new UnityEvent();
    public UnityEvent OnGamePaused = new UnityEvent();
    public UnityEvent OnGameUnpaused = new UnityEvent();

    public NewLevelEvent OnNewLevel = new NewLevelEvent();
    public NewWaveEvent OnNewWave = new NewWaveEvent();
    public UnityEvent OnLevelComplete = new UnityEvent();
    public UnityEvent OnLevelFail = new UnityEvent();
    public ButtonPressEvent OnButtonPress = new ButtonPressEvent();
    public BreadSpawnEvent OnBreadSpawn = new BreadSpawnEvent();
    public BreadGrabbedEvent OnBreadGrabbed = new BreadGrabbedEvent();
    public BreadReleasedEvent OnBreadReleased = new BreadReleasedEvent();
    public IngredientCollisionEvent OnIngredientCollision = new IngredientCollisionEvent();
    public BreadCollisionEvent OnBreadCollision = new BreadCollisionEvent();
    public SandwichMadeEvent OnSandwichMade = new SandwichMadeEvent();
}

// Custom event class declarations inherit from unity event with parameters types
[System.Serializable]
public class ButtonPressEvent : UnityEvent<string> { }

[System.Serializable]
public class NewLevelEvent : UnityEvent<int>{ }

[System.Serializable]
public class NewWaveEvent : UnityEvent<int> { }

[System.Serializable]
public class BreadSpawnEvent : UnityEvent<int> { }

[System.Serializable]
public class BreadGrabbedEvent : UnityEvent<Vector3> { }

[System.Serializable]
public class BreadReleasedEvent : UnityEvent<Vector3> { }

[System.Serializable]
public class IngredientCollisionEvent : UnityEvent<Ingredient, Bread>{ }

[System.Serializable]
public class BreadCollisionEvent : UnityEvent<Bread, Bread> { }

[System.Serializable]
public class SandwichMadeEvent : UnityEvent<Vector3, int> { }
