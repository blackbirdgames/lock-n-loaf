﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Physics raycasts send messages to scripts attached to gameobjects hit
public class TouchManager : MonoBehaviour
{
    public static TouchManager instance;

    public LayerMask touchInputMask;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            DestroyImmediate(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            foreach (Touch touch in Input.touches)
            {
                SendTouchMessages(touch.position, touch.fingerId, touch.phase);
            }
        }

#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            SendTouchMessages(Input.mousePosition, 0, TouchPhase.Began);
            Debug.Log("Mouse button down");
        }
        if (Input.GetMouseButton(0))
        {
            SendTouchMessages(Input.mousePosition, 0, TouchPhase.Moved);
            //Debug.Log("Mouse button held");
        }
        if (Input.GetMouseButtonUp(0))
        {
            SendTouchMessages(Input.mousePosition, 0, TouchPhase.Ended);
            Debug.Log("Mouse button up");
        }
#endif

    }

    // Send touch message to a gameobject on the touchlayer mask
    void SendTouchMessages(Vector3 screenPos, int fingerId, TouchPhase phase)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPos);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, touchInputMask))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.red, 1f);
            GameObject recipient = hit.transform.gameObject;

            TouchData td = new TouchData();
            td.position = hit.point;
            td.touchIndex = fingerId;

            switch (phase)
            {
                case TouchPhase.Began:
                    recipient.SendMessage("OnTouchBegan", td, SendMessageOptions.DontRequireReceiver);
                    break;
                case TouchPhase.Moved:
                    recipient.SendMessage("OnTouchMoved", td, SendMessageOptions.DontRequireReceiver);
                    break;
                case TouchPhase.Stationary:
                    recipient.SendMessage("OnTouchStay", td, SendMessageOptions.DontRequireReceiver);
                    break;
                case TouchPhase.Ended:
                    recipient.SendMessage("OnTouchEnd", td, SendMessageOptions.DontRequireReceiver);
                    break;
                case TouchPhase.Canceled:
                    recipient.SendMessage("OnTouchCancel", td, SendMessageOptions.DontRequireReceiver);
                    break;
            }
        }
    }

    public struct TouchData {
        public Vector3 position;
        public int touchIndex;
    }      
}
